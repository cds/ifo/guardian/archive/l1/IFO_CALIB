from guardian import GuardState, GuardStateDecorator
import time, datetime
#import cdsutils
from isclib.epics_average import EzAvg
import numpy as np
#################################################
request = 'MONITOR'
nominal = request 

# Variables
import ifoconfig # load configuration file holding calirbation line amplitudes

## DEFINITIONS
def changeLineAmp(decriptiveString, ampChannelName, defaultAmplitude, defaultUncertainty, currentUncertainty):
    '''
    Description:
    Changes a calibration line amplitude such that it meets a desired uncertainty level, up to a max to 10x increase
    
    Parameters:
    decriptiveString:   string, String descripbing what the channel is for logging purposes
    ampChannelName:     string, Name of the channel governing the amplitude of a line
    defaultAmplitude:   float,  Value of the default amplitude, as defined by ifoconfig
    defaultUncertainty: float,  Value of target uncertainty, where you want to bring lines to 
    currentUncertainty: float,  Current evaluation of line uncertainty
    '''
    newAmp = ezca[ampChannelName] * currentUncertainty/defaultUncertainty
    if newAmp < (10 * defaultAmplitude):
        log(f'{decriptiveString} line Height Increased by factor {np.round(currentUncertainty/defaultUncertainty, 2)}')
        ezca[ampChannelName] *= currentUncertainty/defaultUncertainty #Would push average uncertainty to defaultUncertainty
    else:
        log(f'{decriptiveString} line Height Increased, to max allowed 10x initial amplitude')
        ezca[ampChannelName] = 10 * defaultAmplitude

## DECORATORS

##################################################
class INIT(GuardState):
    index = 0
    request = True
    goto = True
 
    def main(self):
        return True

###############################################
class MONITOR(GuardState):
    index = 1
    
    def main(self):
        self.uncertTarget    = 0.005
        self.uncertThreshold = 0.009
        self.Pcal16chan = 'CAL-CS_TDEP_PCAL_LINE1_UNCERTAINTY'
        self.Pcal434chan = 'CAL-CS_TDEP_PCAL_LINE2_UNCERTAINTY'
        self.UIMchan = 'CAL-CS_TDEP_SUS_LINE1_UNCERTAINTY'
        self.PUMchan = 'CAL-CS_TDEP_SUS_LINE2_UNCERTAINTY'
        self.TSTchan = 'CAL-CS_TDEP_SUS_LINE3_UNCERTAINTY'
        self.timer['wait'] = 1
        self.notifyString = ''
        self.reintialiseData = True
        self.timer['waitLines'] = 0
        self.timer['leaveObserveFirst'] = 0
        self.changeAmps = False
        self.leaveObserveFirst = False
        return True

    def run(self):
        ##########################################################################################
        # Tertiary function in this state is to check whether the primary Pcal is running properly
        ##########################################################################################
        # Folloing various Pcal incidents, I implement a general health check that will take us out of observing if Pcal Power dies
        # I choose to use the OFS_PD
        # During this incident : https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=71857
        # The OFS PD dipped above -1 V when power was lost briefly before eventually dying to a value just below Zero (its a negative scale)
        if ezca['GRD-ISC_LOCK_STATE_N'] > 1600: #1600 is TURN_ON_CALIBRATION_LINES
            if ifoconfig.use_pcaly == 1:
                pcal_ofs = ezca['CAL-PCALY_OFS_PD_OUTPUT']
            else:
                pcal_ofs = ezca['CAL-PCALX_OFS_PD_OUTPUT']
            if pcal_ofs > -1.0:
                notify('No Pcal Laser detectable')
                # TODO: self-writing aLog code
                return False #Takes us out of observing
        
        ##################################################################################################################
        # Secondary function in this state is to check the Pcal configuration and start transition if settings get changed
        ##################################################################################################################
        # Transition happens in different state to reduce clutter
        if ezca['GRD-ISC_LOCK_STATE_N'] > 1600: # check after calibration lines are turned on.
            PcalY_use_switch = int(round(ezca['CAL-CS_TDEP_PCAL_REF_END_SW'],1)) #rounding and casting to int in case of some floating point imprecision of readout
            PcalY_output = int(round(ezca['CAL-PCALY_OSC_SUM_ON'],1))
            PcalX_output = int(round(ezca['CAL-PCALX_OSC_SUM_ON'],1))
            
            # First ascertain present state relative to intention of ifoconfig
            if ifoconfig.use_pcaly == 1 and PcalY_use_switch == 1 and PcalY_output == 1:
                use_pcaly_correct = True
            elif ifoconfig.use_pcaly == 0 and PcalY_use_switch == 0 and PcalY_output == 0:
                use_pcaly_correct = True
            else:
                use_pcaly_correct = False
            
            if ifoconfig.pcalx_good == 1 and PcalX_output == 1:
                pcalx_good_correct = True
            elif ifoconfig.pcalx_good == 0 and PcalX_output == 0:
                pcalx_good_correct = True
            else:
                pcalx_good_correct = False
            
            if not all([use_pcaly_correct, pcalx_good_correct]):
                return 'TRANSITION_PCAL'
        
        #####################################################################################################
        # Primary function of this state is to change calibration line heights if noise is contaminating them
        #####################################################################################################
        if not self.timer['waitLines']:
            #So that the lines get changed on the same guardian 
            notify('Cal Lines changed. 2mins not observing.')
            if not self.timer['leaveObserveFirst']: # Give plenty of time (1 sec) to leave observing before making changes to avoid any race conditions
                return False
            if self.changeAmps:
                if self.Pcal16 > self.uncertThreshold:
                        changeLineAmp('Pcal16', 'CAL-PCALY_PCALOSC1_OSC_SINGAIN', ifoconfig.pcaly_line1_amp, self.uncertTarget, self.Pcal16)
                if self.UIM > self.uncertThreshold:
                    changeLineAmp('UIM', 'CAL-CS_LINE_1_OSC_CLKGAIN', ifoconfig.calibration_line1_clkgain, self.uncertTarget, self.UIM)
                if self.PUM > self.uncertThreshold:
                    changeLineAmp('PUM', 'CAL-CS_LINE_2_OSC_CLKGAIN', ifoconfig.calibration_line2_clkgain, self.uncertTarget, self.PUM)
                if self.TST > self.uncertThreshold:
                    changeLineAmp('TST', 'CAL-CS_LINE_3_OSC_CLKGAIN', ifoconfig.calibration_line3_clkgain, self.uncertTarget, self.TST)
                if self.Pcal434 < self.uncertThreshold:
                    self.notifyString = 'Cal lines amplitude recently increased'
                self.changeAmps = False
            return False # Takes us out of observe
        
        if ezca['GRD-ISC_LOCK_STATE_N'] >= 2000 and ezca['GRD-IFO_OK'] == 1:
            if self.reintialiseData:
                chans = [self.Pcal16chan, self.Pcal434chan, self.UIMchan, self.PUMchan, self.TSTchan]
                self.medians = EzAvg(ezca,3600+60,chans) #add 1 minute to make sure we make it in the first hour as it seems we always finish and the OPS_OBS_DURATION_HMS is not at the hour mark
                self.reintialiseData = False
            
            [done, medianArray] = self.medians.ezAvg(median=True)
            
            current_low_noise_time = ezca['CDS-OPS_OBS_DURATION_HMS']
            if len(current_low_noise_time.split(',')) > 1:
                current_low_noise_time = current_low_noise_time.split(',')[1]  #fix me if required add 24 hours as well
                current_low_noise_time = current_low_noise_time.split()[0]   # fixes the edge case of " 1 day, 00:00:00" FIXME:amplitude change exactly after 1 day will be delayed 1 hour
            date_format = "%H:%M:%S"
            formatted_time = datetime.datetime.strptime(current_low_noise_time, date_format)
            if done and (formatted_time.hour>=1):
                # Cal Line Uncertainty Checks
                if any(np.array(medianArray) > self.uncertThreshold):
                    log('Uncertainties')
                    self.Pcal16  = medianArray[0]
                    self.Pcal434 = medianArray[1]
                    self.UIM     = medianArray[2]
                    self.PUM     = medianArray[3]
                    self.TST     = medianArray[4]
                    self.notifyString = ''
                    self.leaveObserveFirst = True
                    self.changeAmps = True
                    if self.Pcal434 >= self.uncertThreshold:
                        self.notifyString = 'NOTIFY CAL TEAM NOW. 434 HZ LINE BAD'
                    #################################################################################################################################################
                    # When Line heights are changed, go out of observing for 2minutes,15seconds to let subtraction catch up, so DetChar doesn't need to manually flag
                    #################################################################################################################################################
                    if any(np.array([self.Pcal16,self.UIM,self.PUM,self.TST]) > self.uncertThreshold): #notably without Pcal434
                        timeSinceLastAstroEvent = ezca['CAL-INJ_EXTTRIG_ALERT_TIME']
                        currentTime = ezca['FEC-117_TIME_DIAG']
                        if (currentTime - timeSinceLastAstroEvent) > 900: #if event in last 15 mins, you should be standing down.
                            self.timer['waitLines'] = 120+15+1 #2 mintures plus 15sec ramp time +1 second for leaveObserveFirst
                            self.timer['leaveObserveFirst'] = 1
                        else:
                            pass
                        # TODO: self-writing aLog code
                else:
                    self.notifyString = ''
                self.reintialiseData = True
            elif done and not (formatted_time.hour>=1):
                log('Collected 1 hour of data, but we have not reached 1 hour of observing.')
                self.reintialiseData = True
                self.notifyString = ''
            else:
                pass
        else:# re-start a fresh data collection set on leaving observe to avoid futher race conditions between data collected and OPS_OBS_DURATION_HMS
            self.reintialiseData = True
        
        
        
        
        notify(self.notifyString)
        return True

##################################################
# Prepare IFO for calibration sweeps
##################################################
class GETTING_READY(GuardState):
    index = 5
    request = True

    def main(self):
        notify('Preparing for calibration sweeps')
        ## Turn off all calibration lines
        # Override Subtraction
        for osc in ['1','2','3']:
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc +'_GRM_REAL_OVERRIDE'] = 1

        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 1
        # ASC ADS gain
        #ascads_sp = 0.1 #ezca['ASC-ADS_GAIN']
        ezca['ASC-ADS_GAIN'] = 0
        time.sleep(0.2)

        # SUS LINE L1, L2, L3
        for osc in ['1','2','3']:
            ezca[f'CAL-CS_LINE_{osc}_OSC_TRAMP'] = 15
        time.sleep(0.2)
        for osc in ['1','2','3']:
            ezca[f'CAL-CS_LINE_{osc}_OSC_CLKGAIN'] = 0
        
        # PCAL LINES
        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca[f'CAL-PCALY_PCALOSC{osc}_OSC_TRAMP'] = 15
            ezca[f'CAL-PCALX_PCALOSC{osc}_OSC_TRAMP'] = 15
        time.sleep(0.2)
        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca[f'CAL-PCALY_PCALOSC{osc}_OSC_SINGAIN'] = 0
            ezca[f'CAL-PCALX_PCALOSC{osc}_OSC_SINGAIN'] = 0
        
        # Freeze kappa_{uim,pum,tst} kappa_c and kappa_cc
        ezca['CAL-CS_TDEP_KAPPA_UIM_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_KAPPA_PUM_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_KAPPA_TST_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_CAVITY_POLE_KAPPA_C_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_CAVITY_POLE_F_C_GATE_UNC_THRESH'] = 0

        ## open PCAL swept sine path
        ezca['CAL-PCALY_SWEPT_SINE_ON'] = 1
        ezca['CAL-PCALX_SWEPT_SINE_ON'] = 1
        ezca['CAL-INJ_MASTER_SW'] = 0
        self.timer['wait'] = 15.0 # wait for the ramps

    def run(self):
        if self.timer['wait']:
            return True

####################################################################################################
# Alter configuration of Pcals to calibratrate through either pcalY or X depending on ifoconfig
####################################################################################################
class TRANSITION_PCAL(GuardState):
    index = 4
    request = True

    def main(self):
        notify('Transitioning Pcal calibration configuration')
        self.bothPcalBad = 0
        
        # Override Subtraction
        for osc in ['1','2','3']:
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc +'_GRM_REAL_OVERRIDE'] = 1

        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 1
        
        # Freeze kappa_{uim,pum,tst} kappa_c and kappa_cc
        ezca['CAL-CS_TDEP_KAPPA_UIM_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_KAPPA_PUM_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_KAPPA_TST_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_CAVITY_POLE_KAPPA_C_GATE_UNC_THRESH'] = 0
        ezca['CAL-CS_TDEP_CAVITY_POLE_F_C_GATE_UNC_THRESH'] = 0
        
        pcalRefY = int(round(ezca['CAL-CS_TDEP_PCAL_REF_END_SW'],1))
        if ifoconfig.use_pcaly == 1 and pcalRefY == 0:
            for osc in [1,2,3,4,5,6,7,8,9]:
                ezca[f'CAL-PCALX_PCALOSC{osc}_OSC_TRAMP'] = 15
                ezca[f'CAL-PCALY_PCALOSC{osc}_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_OSC_SUM_TRAMP'] = 15
            ezca['CAL-PCALX_OSC_SUM_TRAMP'] = 15
            time.sleep(0.1)
            for osc in ['1','2','3','4','5','6','7','8','9']:
                ezca['CAL-PCALY_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
                ezca['CAL-PCALX_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_1'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_2'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_3'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_4'] = 1
            if ifoconfig.pcalx_good: 
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_5'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_6'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_7'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_8'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_9'] = 1
            time.sleep(0.1)
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 1
            ezca['CAL-PCALX_RX_PD_GAIN'] = 1 # Re-correct gain if we had switched over to PcalX previously
            if ifoconfig.pcalx_good:
                ezca['CAL-INJ_MASTER_SW'] = 1
            else:
                ezca['CAL-INJ_MASTER_SW'] = 0
            ezca['CAL-PCALY_PCALOSC1_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_PCALOSC2_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_PCALOSC3_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_PCALOSC4_OSC_TRAMP'] = 15
            if ifoconfig.pcalx_good:
                ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 15 # HF line
                ezca['CAL-PCALX_PCALOSC5_OSC_TRAMP'] = 15
                ezca['CAL-PCALX_PCALOSC6_OSC_TRAMP'] = 15
                ezca['CAL-PCALX_PCALOSC7_OSC_TRAMP'] = 15
                ezca['CAL-PCALX_PCALOSC8_OSC_TRAMP'] = 15
                ezca['CAL-PCALX_PCALOSC9_OSC_TRAMP'] = 15
            time.sleep(0.1)
            ezca['CAL-PCALY_PCALOSC4_OSC_FREQ'] = ifoconfig.pcaly_line4_freq
            ezca['CAL-PCALY_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALY_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALY_PCALOSC1_OSC_FREQ'] = ifoconfig.pcaly_line1_freq
            ezca['CAL-PCALY_PCALOSC4_OSC_SINGAIN'] = ifoconfig.pcaly_line4_amp
            ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
            ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
            if ifoconfig.pcalx_good: 
                ezca['CAL-PCALX_PCALOSC5_OSC_FREQ'] = ifoconfig.pcalx_line5_freq
                ezca['CAL-PCALX_PCALOSC6_OSC_FREQ'] = ifoconfig.pcalx_line6_freq
                ezca['CAL-PCALX_PCALOSC7_OSC_FREQ'] = ifoconfig.pcalx_line7_freq
                ezca['CAL-PCALX_PCALOSC8_OSC_FREQ'] = ifoconfig.pcalx_line8_freq
                ezca['CAL-PCALX_PCALOSC9_OSC_FREQ'] = ifoconfig.pcalx_line9_freq
                ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcalx_line1_amp #freq handled by HF guardian
                ezca['CAL-PCALX_PCALOSC5_OSC_SINGAIN'] = ifoconfig.pcalx_line5_amp
                ezca['CAL-PCALX_PCALOSC6_OSC_SINGAIN'] = ifoconfig.pcalx_line6_amp
                ezca['CAL-PCALX_PCALOSC7_OSC_SINGAIN'] = ifoconfig.pcalx_line7_amp
                ezca['CAL-PCALX_PCALOSC8_OSC_SINGAIN'] = ifoconfig.pcalx_line8_amp
                ezca['CAL-PCALX_PCALOSC9_OSC_SINGAIN'] = ifoconfig.pcalx_line9_amp
            time.sleep(0.1)
            ezca['CAL-PCALX_OSC_SUM_ON'] = 1
            if ifoconfig.pcalx_good: 
                ezca['CAL-PCALY_OSC_SUM_ON'] = 1
            else:
                ezca['CAL-PCALY_OSC_SUM_ON'] = 0
        elif ifoconfig.use_pcaly == 0 and ifoconfig.pcalx_good == 1 and pcalRefY == 1:
            for osc in [1,2,3,4,5,6,7,8,9]:
                ezca[f'CAL-PCALX_PCALOSC{osc}_OSC_TRAMP'] = 15
                ezca[f'CAL-PCALY_PCALOSC{osc}_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_OSC_SUM_TRAMP'] = 15
            ezca['CAL-PCALX_OSC_SUM_TRAMP'] = 15
            time.sleep(0.1)
            for osc in ['1','2','3','4','5','6','7','8','9']:
                ezca['CAL-PCALY_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
                ezca['CAL-PCALX_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 0
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_1'] = 1
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_2'] = 1
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_3'] = 1
            time.sleep(0.1)
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 0
            ezca['CAL-PCALX_RX_PD_GAIN'] = -1 # Flip polarity to match what Y arm would be
            ezca['CAL-INJ_MASTER_SW'] = 0
            ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 15
            ezca['CAL-PCALX_PCALOSC2_OSC_TRAMP'] = 15
            ezca['CAL-PCALX_PCALOSC3_OSC_TRAMP'] = 15
            time.sleep(0.1)
            ezca['CAL-PCALX_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALX_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = ifoconfig.pcaly_line1_freq
            ezca['CAL-PCALX_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALX_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp 
            ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
            time.sleep(0.1)
            ezca['CAL-PCALX_OSC_SUM_ON'] = 1
            ezca['CAL-PCALY_OSC_SUM_ON'] = 0
        else:
            self.bothPcalBad = 1
        
        for osc in ['1','2','3']:
            ezca['CAL-CS_TDEP_SUS_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
        # Unfreeze kappa_c and kappa_cc
        ezca['CAL-CS_TDEP_KAPPA_UIM_GATE_UNC_THRESH'] = 0.09
        ezca['CAL-CS_TDEP_KAPPA_PUM_GATE_UNC_THRESH'] = 0.09
        ezca['CAL-CS_TDEP_KAPPA_TST_GATE_UNC_THRESH'] = 0.09
        ezca['CAL-CS_TDEP_CAVITY_POLE_KAPPA_C_GATE_UNC_THRESH'] = 0.09
        ezca['CAL-CS_TDEP_CAVITY_POLE_F_C_GATE_UNC_THRESH'] = 0.09
        self.timer['wait'] = 15.0 # wait for the ramps

    def run(self):
        if self.bothPcalBad:
            notify('Config cannot use either Pcal. Contact CAL team')
            return False
        if self.timer['wait']:
            #After ramps toggle the shutter. This causes a glitch
            if ifoconfig.use_pcaly == 1:
                ezca['CAL-PCALY_SHUTTERPOWERENABLE'] = 1
            else:
                ezca['CAL-PCALY_SHUTTERPOWERENABLE'] = 0
            #sleep 10 second after glitch
            time.sleep(10)
            
            if ifoconfig.pcalx_good == 1:
                ezca['CAL-PCALX_SHUTTERPOWERENABLE'] = 1
            else:
                ezca['CAL-PCALX_SHUTTERPOWERENABLE'] = 0
            #sleep 10 second after glitch
            time.sleep(10)
            return 'MONITOR'

##################################################
# Prepare IFO for Specific cases that need calibration lines off
##################################################
class HW_INJ(GuardState):
    index = 6
    request = True

    def main(self):
        time.sleep(0.1)
        ezca['CAL-INJ_MASTER_SW'] = 1

    def run(self):
        return True

class ADS_ONLY(GuardState):
    index = 7
    request = True

    def main(self):
        time.sleep(0.1)
        ezca['ASC-ADS_GAIN'] = 3.0

    def run(self):
        return True

#################################################
# Calibration Complete -- Restore IFO settings
#################################################
class COMPLETE_CALIBRATION(GuardState):
    index = 55
    request = True

    def main(self):
        # set PCal lines amplitudes to their original values
        # TRAMP of 15 sec is already set
        if ifoconfig.use_pcaly == 1:
            ezca['CAL-PCALY_PCALOSC4_OSC_SINGAIN'] = ifoconfig.pcaly_line4_amp
            ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
            ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
            if ifoconfig.pcalx_good:
                ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcalx_line1_amp
                ezca['CAL-PCALX_PCALOSC5_OSC_SINGAIN'] = ifoconfig.pcalx_line5_amp
                ezca['CAL-PCALX_PCALOSC6_OSC_SINGAIN'] = ifoconfig.pcalx_line6_amp
                ezca['CAL-PCALX_PCALOSC7_OSC_SINGAIN'] = ifoconfig.pcalx_line7_amp
                ezca['CAL-PCALX_PCALOSC8_OSC_SINGAIN'] = ifoconfig.pcalx_line8_amp
                ezca['CAL-PCALX_PCALOSC9_OSC_SINGAIN'] = ifoconfig.pcalx_line9_amp
            if ifoconfig.pcalx_good:
                ezca['CAL-INJ_MASTER_SW'] = 1
            else:
                ezca['CAL-INJ_MASTER_SW'] = 0
        else:
            ezca['CAL-PCALX_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALX_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
            ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
            ezca['CAL-INJ_MASTER_SW'] = 0
        
        ezca['CAL-CS_LINE_1_OSC_CLKGAIN'] = ifoconfig.calibration_line1_clkgain
        ezca['CAL-CS_LINE_2_OSC_CLKGAIN'] = ifoconfig.calibration_line2_clkgain
        ezca['CAL-CS_LINE_3_OSC_CLKGAIN'] = ifoconfig.calibration_line3_clkgain

        # set ASC ADS to value before taking sweeps
        #ezca['ASC-ADS_GAIN'] = ascads_sp
        ezca['ASC-ADS_GAIN'] = 3.0 #0.02 #3 is in the current SDF

        # close PCAL swept sine path
        ezca['CAL-PCALY_SWEPT_SINE_ON'] = 0
        ezca['CAL-PCALX_SWEPT_SINE_ON'] = 0
        
        
        self.timer['wait'] = 15.0 # wait for the ramps

    def run(self):
        if self.timer['wait']:
            for osc in ['1','2','3']:
                ezca['CAL-CS_TDEP_SUS_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
            for osc in ['1','2','3','4','5','6','7','8','9']:
                ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
            # Unfreeze kappa_c and kappa_cc
            ezca['CAL-CS_TDEP_KAPPA_UIM_GATE_UNC_THRESH'] = 0.09
            ezca['CAL-CS_TDEP_KAPPA_PUM_GATE_UNC_THRESH'] = 0.09
            ezca['CAL-CS_TDEP_KAPPA_TST_GATE_UNC_THRESH'] = 0.09
            ezca['CAL-CS_TDEP_CAVITY_POLE_KAPPA_C_GATE_UNC_THRESH'] = 0.09
            ezca['CAL-CS_TDEP_CAVITY_POLE_F_C_GATE_UNC_THRESH'] = 0.09
            notify('Calibration sweeps done! OAF-CAL subtraction reinstated.')
            return True

###################################################
edges = [
    ('INIT','MONITOR'),
    ('MONITOR','MONITOR'),
    ('MONITOR','TRANSITION_PCAL'),
    ('TRANSITION_PCAL','TRANSITION_PCAL'),
    ('TRANSITION_PCAL','MONITOR'),
    ('MONITOR','GETTING_READY'),
    ('GETTING_READY','COMPLETE_CALIBRATION'),
    ('GETTING_READY','HW_INJ'),
    ('GETTING_READY','ADS_ONLY'),
    ('HW_INJ','COMPLETE_CALIBRATION'),
    ('ADS_ONLY','COMPLETE_CALIBRATION'),
    ('COMPLETE_CALIBRATION','MONITOR')
    ]
